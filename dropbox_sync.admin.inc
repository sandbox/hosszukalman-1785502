<?php
/**
 * @file
 * Admin section's functions.
 */

function dropbox_sync_admin_page() {
  $build = array();

  // Success handshake returns with the toke.
  if (isset($_GET['oauth_token'])) {
    variable_set('dropbox_sync_oauth_token', $_GET['oauth_token']);
  }
  // Start the synchronization.
  elseif (isset($_GET['syncnow'])) {
    $data = array(
      'oauth_consumer_key' => 'bzstv1litxp2taa',
      'oauth_signature_method' => 'PLAINTEXT',
      'oauth_signature' => 'qkqwaso2kfbft9h&',
      'oauth_version' => '1.0',
    );
    $options = array(
      'method' => 'GET',
    );
    $result = drupal_http_request('https://api.dropbox.com/1/oauth/request_token?' . http_build_query($data), $options);

    // If the result code is ok, redirects the user to the authorize page.
    if ($result->code == 200) {
      parse_str($result->data, $params);
      $params['oauth_callback'] = url('admin/config/services/dropbox', array('absolute' => TRUE));

      drupal_goto('https://www.dropbox.com/1/oauth/authorize', array('query' => $params));
    }
    else {
      // Handling error message.
      $res_data = json_decode($result->data);
      drupal_set_message($res_data->error, 'error');
      watchdog('Dropbox sync', $res_data->error, array(), WATCHDOG_ERROR);
    }
  }

  // Check if the oauth token is set and create markups with the correct
  // messages and links to the synchronization.
  if (variable_get('dropbox_sync_oauth_token')) {
    $build[] = array(
      '#markup' => t('The Dropbox account is set. If you want to set another account click !url',
              array('!url' => l(t('here'), 'admin/config/services/dropbox',
                array('query' => array('syncnow' => '1'))))),
    );
  }
  else {
    $build[] = array(
      '#markup' => t('The Dropbox account is not set. If you want to set the account click !url',
        array('!url' => l(t('here'), 'admin/config/services/dropbox',
          array('query' => array('syncnow' => '1'))))),
    );
  }

  return $build;
}
